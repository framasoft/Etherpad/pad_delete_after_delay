# INSTALLATION

**Warning!**

* You will still need to install the plugin `ep_delete_after_delay` on your etherpad instance if you want to be sure that pads that expired between two check loops can't be accessed.

## Dependencies

### Carton

Carton is a Perl dependencies manager, it will get what you need, so don't bother for Perl modules dependencies (but you can read the file `cpanfile` if you want).

```shell
sudo cpan Carton
```

Some modules that Carton will install need to be compiled. So you will need some tools:

```shell
sudo apt-get install build-essential libssl-dev
```

## Installation

After installing Carton:

```shell
cd /opt
git clone https://framagit.org/framasoft/pad_delete_after_delay.git delete_after_delay
cd delete_after_delay
carton install
sudo cp delete_after_delay.yml.template /etc/delete_after_delay.yml
sudo chmod 640 /etc/delete_after_delay.yml
sudo vi /etc/delete_after_delay.yml
```

The `dir` setting is really important: when a pad is expired, `delete_after_delay` stores a HTML copy of the pad in `$dir/deleted_pads`.
The plugin `ep_delete_after_delay` does the same thing, in the etherpad installation directory.
You can choose to set `dir` to the etherpad installation directory but then you will need to be sure that the `deleted_pads` is writable by the etherpad user AND the user you run `delete_after_delay` with (you can change it in `delete_after_delay@.service` with the `User` setting if you run it as a systemd service).

Warning! Verify that the expiration delays in `/etc/delete_after_delay.yml` and in your etherpad instance's `settings.json` are the same!

Check that `loop` setting is set to `false` in your etherpad instance's `settings.json`.

### delete\_after\_delay

You can see the options of `delete_after_delay` with:

```shell
/opt/delete_after_delay/delete_after_delay -h
```

If you want to start `delete_after_delay` as a systemd service:

```shell
sudo cp delete_after_delay@.service /etc/systemd/system/delete_after_delay@.service
# Change User in /etc/systemd/system/delete_after_delay@.service in needed
sudo systemctl daemon-reload
# To start delete_after_delay on instance foo:
sudo systemctl start delete_after_delay@foo.service
# To start at boot:
sudo systemctl enable delete_after_delay@foo.service
```

Edit `/etc/systemd/system/delete_after_delay@.service` to change the delay between check loops if you want.
