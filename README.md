[![](https://framagit.org/assets/favicon-075eba76312e8421991a0c1f89a89ee81678bcde72319dd3e8047e2a47cd3a42.ico)](https://framagit.org)

![English:](https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Flag_of_the_United_Kingdom.svg/20px-Flag_of_the_United_Kingdom.svg.png) **Framasoft uses GitLab** for the development of its free softwares. Our Github repositories are only mirrors.
If you want to work with us, **fork us on [framagit.org](https://framagit.org)**. (no registration needed, you can sign in with your Github account)

![Français :](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Flag_of_France.svg/20px-Flag_of_France.svg.png) **Framasoft utilise GitLab** pour le développement de ses logiciels libres. Nos dépôts Github ne sont que des miroirs.
Si vous souhaitez travailler avec nous, **forkez-nous sur [framagit.org](https://framagit.org)**. (l'inscription n'est pas nécessaire, vous pouvez vous connecter avec votre compte Github)
* * *

# PAD\_DELETE\_AFTER\_DELAY

At [Framasoft](https://framasoft.org), we host a lot of heavily-used [Etherpad](http://etherpad.org) instances.

We wanted to have time-limited pads, so [Luc Didry](https://luc.frama.io) developped a [plugin](https://framagit.org/luc/ep_delete_after_delay) which deletes pads after a configured delay.

But we have so many pads (more than 30k on some instances) that the plugin fails to work correctly: it causes a really huge load on the etherpad processus at start and stuck it.

So we developped an external script to delete the expired pads without loading the etherpad processus.

`delete_after_delay` asks an etherpad instance for the list of its pads, then check (and delete if expired) all of them.

# Installation

Have a look at the [INSTALL.md file](INSTALL.md)

# License

GNU General Public License, version 3. Check the [LICENSE file](LICENSE).
